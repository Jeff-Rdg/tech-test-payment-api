﻿
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Payment_API.Models;
using Payment_API.Repository.Interfaces;
using Payment_API.ViewModel;

namespace Payment_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SellerController : ControllerBase
    {
        private readonly ISellerRepository _sellerRepository;
        private readonly IMapper _mapper;

        public SellerController(ISellerRepository sellerRepository, IMapper mapper)
        {
            _sellerRepository = sellerRepository;
            _mapper = mapper;
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<Seller>>> GetAll()
        {
            var sellers = await _sellerRepository.GetAll();
            if (sellers == null)
            {
                return NotFound();
            }
            return Ok(sellers);
        }


        [HttpGet("{id}")]
        public ActionResult<Seller> GetById(int id)
        {
            var seller = _sellerRepository.GetById(id);
            if (seller == null)
            {
                return NotFound();
            }
            return Ok(seller);
        }


        [HttpPost]
        public ActionResult<Seller> Create(SellerViewModel sellerModel)
        {
            var seller = _mapper.Map<SellerViewModel, Seller>(sellerModel);

            _sellerRepository.Create(seller);
            return CreatedAtAction(nameof(GetById), new { id = seller.Id }, seller);
        }


        [HttpPut("{id}")]
        public async Task<ActionResult<Seller>> Update(int id, [FromBody] Seller seller)
        {
            if (seller.Id == id)
            {
                await _sellerRepository.Update(seller);
                return Ok($"Vendedor com Id: {id} atualizado com sucesso!");
            }
            else
            {
                return BadRequest();
            }
        }


        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var seller = _sellerRepository.GetById(id);
            if (seller == null)
            {
                return NotFound("Vendedor não encontrado!");
            }
            else
            {
                await _sellerRepository.Delete(seller);
                return Ok($"Vendedor com Id: {id} deletado com sucesso!");
            }
        }
    }
}
