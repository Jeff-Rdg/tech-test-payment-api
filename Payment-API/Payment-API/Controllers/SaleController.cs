﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Payment_API.Models;
using Payment_API.Repository.Interfaces;
using Payment_API.ViewModel;

namespace Payment_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SaleController : ControllerBase
    {
        private readonly ISaleRepository _saleRepository;
        private readonly ISellerRepository _sellerRepository;
        private readonly IMapper _mapper;
        private readonly ISaleItemRepository _saleItemRepository;
        private readonly IProductRepository _productRepository;



        public SaleController(
            ISaleRepository saleRepository, IMapper mapper, ISellerRepository sellerRepository, ISaleItemRepository saleItemRepository, IProductRepository productRepository)
        {
            _saleRepository = saleRepository;
            _mapper = mapper;
            _sellerRepository = sellerRepository;
            _saleItemRepository = saleItemRepository;
            _productRepository = productRepository;

        }

        [HttpGet("{id}")]
        public ActionResult<Sale> GetById(int id)
        {
            var sale = _saleRepository.GetById(id);
            if (sale == null)
            {
                return NotFound();
            }
            return Ok(sale);
        }


        [HttpPost]
        public ActionResult<Sale> Create(SaleViewModel saleModel)
        {
            try
            {
                // validação se o vendedor existe
                var seller = _sellerRepository.GetById(saleModel.SellerId);
                if (seller == null)
                {
                    return NotFound("Vendedor não existe.");
                }
                // validação se os produtos existem
                foreach (var item in saleModel.SaleItems)
                {
                    var product = _productRepository.GetById(item.ProductId);
                    if (product == null)
                    {
                        return NotFound($"Produto com id {product.Id} não existe.");
                    }
                }
                // crio um objeto do tipo Sale
                Sale sale = new()
                {
                    SellerId = saleModel.SellerId,
                    DateSale = DateTime.Now,
                    Status = enums.SaleStatus.AwaitingPayment,
                    SaleItems = saleModel.SaleItems,
                };

                _saleRepository.Create(sale);
                return CreatedAtAction(nameof(GetById), new { id = sale.Id }, sale);

            }
            catch
            {

                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var sale = _saleRepository.GetById(id);
            if (sale == null)
            {
                return NotFound("Venda não encontrada!");
            }
            else
            {
                await _saleRepository.Delete(sale);
                return Ok($"Venda com Id: {id} deletado com sucesso!");
            }
        }

        [HttpPut("/PaymentApproval/{id}")]
        public ActionResult PaymentApproval(int id)
        {
            Sale sale = _saleRepository.GetById(id);
            if (sale == null)
            {
                return NotFound("Venda não encontrada!");
            }

            if (sale.Status.Equals(enums.SaleStatus.PaymentApproved))
            {
                return BadRequest($"A venda com Id:{id} já está com pagamento aprovado.");
            }

            if (sale.Status.Equals(enums.SaleStatus.AwaitingPayment))
            {
                sale.Status = enums.SaleStatus.PaymentApproved;
                _saleRepository.Update(sale);
                return Ok($"Pagamento da venda com Id:{id} aprovado com sucesso.");
            }
            return BadRequest("Operação inválida");
        }

        [HttpPut("/Cancel/{id}")]
        public ActionResult CancelTransaction(int id)
        {
            Sale sale = _saleRepository.GetById(id);
            if (sale == null)
                return NotFound("Venda não encontrada!");

            if (sale.Status.Equals(enums.SaleStatus.Canceled))
            {
                return BadRequest($"A venda com Id:{id} já está cancelada.");
            }

            if (sale.Status.Equals(enums.SaleStatus.AwaitingPayment) || sale.Status.Equals(enums.SaleStatus.PaymentApproved))
            {
                sale.Status = enums.SaleStatus.Canceled;
                _saleRepository.Update(sale);
                return Ok($"Venda com Id:{id} cancelada com sucesso.");
            }
            else
            {
                return BadRequest("Operação Inválida.");
            }
        }

        [HttpPut("/GoToTransporter/{id}")]
        public ActionResult GoToTransporter(int id)
        {
            Sale sale = _saleRepository.GetById(id);
            if (sale == null)
                return NotFound("Venda não encontrada!");

            if (sale.Status.Equals(enums.SaleStatus.SentToTransporter))
            {
                return BadRequest($"A venda com Id:{id} já foi enviada para transportadora.");
            }

            if (!sale.Status.Equals(enums.SaleStatus.PaymentApproved))
                return BadRequest($"Não é possivel realizar esta Operação para a venda com Id: {id}");

            sale.Status = enums.SaleStatus.SentToTransporter;

            _saleRepository.Update(sale);
            return Ok($"Venda com Id:{id} Enviada para transportadora com sucesso.");
        }

        [HttpPut("/Delivered/{id}")]
        public ActionResult Delivered(int id)
        {
            Sale sale = _saleRepository.GetById(id);
            if (sale == null)
                return NotFound("Venda não encontrada!");

            if (sale.Status.Equals(enums.SaleStatus.SentToTransporter))
            {
                sale.Status = enums.SaleStatus.Delivered;
                _saleRepository.Update(sale);
                return Ok($"Venda com Id:{id} Entregue ao destinatário com sucesso.");

            }

            return BadRequest($"Não é possivel realizar esta Operação para a venda com Id: {id}");
        }
    }
}
