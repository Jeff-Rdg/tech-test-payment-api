﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Payment_API.Models;
using Payment_API.Repository.Interfaces;
using Payment_API.ViewModel;

namespace Payment_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductRepository _productRepository;
        private readonly IMapper _mapper;

        public ProductController(IProductRepository productRepository, IMapper mapper)
        {
            _productRepository = productRepository;
            _mapper = mapper;
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<Product>>> GetAll()
        {
            var products = await _productRepository.GetAll();
            if (products == null)
            {
                return NotFound();
            }
            return Ok(products);
        }


        [HttpGet("{id}")]
        public ActionResult<Product> GetById(int id)
        {
            var product = _productRepository.GetById(id);
            if (product == null)
            {
                return NotFound("Não existe produto com o Id informado.");
            }
            return Ok(product);
        }


        [HttpPost]
        public ActionResult<Product> Create(ProductViewModel productView)
        {
            var product = _mapper.Map<ProductViewModel, Product>(productView);

            _productRepository.Create(product);
            return CreatedAtAction(nameof(GetById), new { id = product.Id }, product);
        }


        [HttpPut("{id}")]
        public async Task<ActionResult<Product>> Update(int id, [FromBody] Product product)
        {
            if (product.Id == id)
            {
                await _productRepository.Update(product);
                return Ok($"Produto com Id: {id} atualizado com sucesso!");
            }
            else
            {
                return BadRequest();
            }
        }


        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var product = _productRepository.GetById(id);
            if (product == null)
            {
                return NotFound("Produto não encontrado!");
            }
            else
            {
                await _productRepository.Delete(product);
                return Ok($"Produto com Id: {id} deletado com sucesso!");
            }
        }
    }
}
