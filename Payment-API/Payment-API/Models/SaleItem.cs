﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Payment_API.Models
{
    public class SaleItem
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        [JsonIgnore]
        public Product? Product { get; set; }
        public int Quantity { get; set; }
        public decimal Value { get; set; }
        public int SaleId { get; set; }
    }
}
