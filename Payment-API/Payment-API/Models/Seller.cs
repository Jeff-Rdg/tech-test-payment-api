﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Payment_API.Models
{
    public class Seller
    {
        public int Id { get; set; }
        [Required]
        [StringLength(200 , MinimumLength = 5)]
        public string Name { get; set; } = null!;
        [Required]
        [StringLength(11, MinimumLength = 11)]
        public string Cpf { get; set; } = null!;
        [Required]
        [StringLength(100)]
        [EmailAddress]
        public string Email { get; set; } = null!;
        [Phone]
        [Required]
        [StringLength(11, MinimumLength = 11)]
        public string Phone { get; set; } = null!;
        [JsonIgnore(Condition = JsonIgnoreCondition.Always)]
        public ICollection<Sale> Sale { get; set; }
    }
}
