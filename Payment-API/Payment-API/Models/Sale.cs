﻿using Payment_API.enums;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Payment_API.Models
{
    public class Sale
    {
        public int Id { get; set; }
        [Required]
        public int SellerId { get; set; }
        [JsonIgnore]
        public Seller? Seller { get; set; }
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public SaleStatus Status { get; set; } = SaleStatus.AwaitingPayment;
        public DateTime DateSale { get; set; } = DateTime.Now;
        public virtual IEnumerable<SaleItem> SaleItems { get; set; }
    }
}
