﻿using AutoMapper;
using Payment_API.Models;
using Payment_API.ViewModel;

namespace Payment_API.DTO.Profiles
{
    public class ResourceToModelProfile : Profile
    {
        public ResourceToModelProfile()
        {
            CreateMap<Product, ProductViewModel>();
        }
    }
}
