﻿using AutoMapper;
using Payment_API.Models;
using Payment_API.ViewModel;

namespace Payment_API.DTO.Profiles
{
    public class ModelToResourceProfile : Profile
    {

        public ModelToResourceProfile()
        {
            CreateMap<ProductViewModel, Product>();
            CreateMap<SellerViewModel, Seller>();
            CreateMap<SaleViewModel, Sale>();
        }
    }
}
