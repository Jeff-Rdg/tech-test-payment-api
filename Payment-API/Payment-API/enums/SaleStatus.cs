﻿using System.ComponentModel;
using System.Text.Json.Serialization;

namespace Payment_API.enums
{
    public enum SaleStatus
    {
        [Description("Aguardando Pagamento")]
        AwaitingPayment,
        [Description("Pagamento Aprovado")]
        PaymentApproved,
        [Description("Cancelado")]
        Canceled,
        [Description("Enviado para transportadora")]
        SentToTransporter,
        [Description("Entregue")]
        Delivered
    }
}
