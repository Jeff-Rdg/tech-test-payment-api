﻿using Microsoft.EntityFrameworkCore;
using Payment_API.Data;
using Payment_API.Models;
using Payment_API.Repository.Interfaces;

namespace Payment_API.Repository
{
    public class SellerRepository : BaseRepository<Seller>, ISellerRepository
    {
        private readonly PaymentContext _context;
        public SellerRepository(PaymentContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Seller>> GetAll()
        {
            var sellers = await _context.Sellers.Include(x => x.Sale).ToListAsync();
            return sellers;
        }

        public Seller GetById(int id)
        {
            var seller = _context.Sellers.Include(x => x.Sale).FirstOrDefault(p => p.Id == id);
            return seller;
        }
    }
}
