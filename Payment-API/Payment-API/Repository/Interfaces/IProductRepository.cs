﻿using Payment_API.Models;

namespace Payment_API.Repository.Interfaces
{
    public interface IProductRepository : IBaseRepository<Product>
    {
        Task<IEnumerable<Product>> GetAll();
        Product GetById(int id);
    }
}
