﻿using Payment_API.Data;
using Payment_API.Models;

namespace Payment_API.Repository.Interfaces
{
    public interface ISellerRepository : IBaseRepository<Seller>
    {
        Task<IEnumerable<Seller>> GetAll();
        Seller GetById(int id);
    }
}
