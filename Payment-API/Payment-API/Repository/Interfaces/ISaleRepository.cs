﻿using Payment_API.Models;

namespace Payment_API.Repository.Interfaces
{
    public interface ISaleRepository : IBaseRepository<Sale>
    {
        Sale GetById(int id);
        void SaleStatusUpdate(Sale sale);

    }
}
