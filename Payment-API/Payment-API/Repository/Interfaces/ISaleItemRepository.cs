﻿using Payment_API.Models;

namespace Payment_API.Repository.Interfaces
{
    public interface ISaleItemRepository : IBaseRepository<SaleItem>
    {
    }
}
