﻿using Payment_API.Models;

namespace Payment_API.Repository.Interfaces
{
    public interface IBaseRepository<T> where T : class
    {
        void Create(T entity);
        Task Update(T entity);
        Task Delete(T entity);
    }
}
