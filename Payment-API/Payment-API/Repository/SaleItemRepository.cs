﻿using Payment_API.Data;
using Payment_API.Models;
using Payment_API.Repository.Interfaces;

namespace Payment_API.Repository
{
    public class SaleItemRepository : BaseRepository<SaleItem>, ISaleItemRepository
    {
        public SaleItemRepository(PaymentContext context) : base(context)
        {
        }
    }
}
