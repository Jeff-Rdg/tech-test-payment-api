﻿using Microsoft.EntityFrameworkCore;
using Payment_API.Data;
using Payment_API.Models;
using Payment_API.Repository.Interfaces;

namespace Payment_API.Repository
{
    public class ProductRepository : BaseRepository<Product>, IProductRepository 
    {
        private readonly PaymentContext _context;
        public ProductRepository(PaymentContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Product>> GetAll()
        {
            var products = await _context.Products.ToListAsync();
            return products;
        }

        public Product GetById(int id)
        {
            var product = _context.Products.FirstOrDefault(p => p.Id == id);
            return product;
        }
    }
}
