﻿using Microsoft.EntityFrameworkCore;
using Payment_API.Data;
using Payment_API.Repository.Interfaces;

namespace Payment_API.Repository
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        private readonly PaymentContext _context;

        public BaseRepository(PaymentContext context)
        {
            _context = context;
        }

        public void Create(T entity)
        {
            try
            {
                _context.Set<T>().Add(entity);
                _context.SaveChanges();

            }
            catch 
            {

                throw;
            }
        }

        public async Task Update(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        public async Task Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
            await _context.SaveChangesAsync();
        }
    }
}
