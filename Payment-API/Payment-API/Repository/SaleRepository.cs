﻿using Microsoft.EntityFrameworkCore;
using Payment_API.Data;
using Payment_API.enums;
using Payment_API.Models;
using Payment_API.Repository.Interfaces;

namespace Payment_API.Repository
{
    public class SaleRepository : BaseRepository<Sale>, ISaleRepository
    {
        private readonly PaymentContext _context;
        public SaleRepository(PaymentContext context) : base(context)
        {
            _context = context;
        }

        public Sale GetById(int id)
        {
            var sale = _context.Sales.Include(x => x.Seller).Include(x=>x.SaleItems.Where(x=>x.SaleId == id)).FirstOrDefault(p => p.Id == id);
            return sale;
        }

        public void SaleStatusUpdate(Sale sale)
        {
            _context.Sales.Update(sale);
            _context.SaveChangesAsync();
        }
    }
}
