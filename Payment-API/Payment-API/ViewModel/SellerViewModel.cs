﻿using System.ComponentModel.DataAnnotations;

namespace Payment_API.ViewModel
{
    public class SellerViewModel
    {
        [Required]
        [StringLength(200, MinimumLength = 5)]
        public string Name { get; set; } = null!;
        [Required]
        [StringLength(11, MinimumLength = 11)]
        public string Cpf { get; set; } = null!;
        [Required]
        [StringLength(100)]
        [EmailAddress]
        public string Email { get; set; } = null!;
        [Phone]
        [Required]
        [StringLength(11, MinimumLength = 11)]
        public string Phone { get; set; } = null!;
    }
}
