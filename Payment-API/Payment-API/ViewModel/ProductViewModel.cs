﻿using System.ComponentModel.DataAnnotations;

namespace Payment_API.ViewModel
{
    public class ProductViewModel
    {
        [Required]
        [StringLength(100)]
        public string Name { get; set; } = null!;
    }
}
