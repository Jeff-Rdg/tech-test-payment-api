﻿using Payment_API.Models;
using System.ComponentModel.DataAnnotations;

namespace Payment_API.ViewModel
{
    public class SaleViewModel
    {
        [Required]
        public int SellerId { get; set; }
        public IEnumerable<SaleItem> SaleItems { get; set; }
    }
}
